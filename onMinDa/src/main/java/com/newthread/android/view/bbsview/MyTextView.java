package com.newthread.android.view.bbsview;

import android.content.Context;
import android.graphics.Color;
import android.text.*;
import android.text.method.LinkMovementMethod;
import android.text.style.BackgroundColorSpan;
import android.text.style.ClickableSpan;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

public class MyTextView extends TextView {

    private Context mContext;

    public MyTextView(Context context) {
        super(context);
        mContext = context;
    }

    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    private int mStart = -1;

    private int mEnd = -1;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean result = super.onTouchEvent(event);

        int action = event.getAction();

        // if (action == MotionEvent.ACTION_UP || action ==
        // MotionEvent.ACTION_DOWN) {
        int x = (int) event.getX();
        int y = (int) event.getY();

        x -= getTotalPaddingLeft();
        y -= getTotalPaddingTop();

        x += getScrollX();
        y += getScrollY();

        Layout layout = getLayout();
        int line = layout.getLineForVertical(y);
        int off = layout.getOffsetForHorizontal(line, x);

        CharSequence text = getText();
        if (TextUtils.isEmpty(text) || !(text instanceof Spannable)) {
            return result;
        }

        Spannable buffer = (Spannable) text;
        ClickableSpan[] link = buffer.getSpans(off, off, ClickableSpan.class);

        if (link.length != 0) {

            if (action == MotionEvent.ACTION_DOWN) {

                mStart = buffer.getSpanStart(link[0]);
                mEnd = buffer.getSpanEnd(link[0]);

                if (mStart >= 0 && mEnd >= mStart) {
                    buffer.setSpan(new BackgroundColorSpan(Color.GRAY), mStart, mEnd,
                            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            } else if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL) {

                if (mStart >= 0 && mEnd >= mStart) {
                    buffer.setSpan(new BackgroundColorSpan(Color.TRANSPARENT), mStart, mEnd,
                            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                    mStart = -1;
                    mEnd = -1;
                }
            }

            return true;
        } else {
            if (mStart >= 0 && mEnd >= mStart) {
                buffer.setSpan(new BackgroundColorSpan(Color.TRANSPARENT), mStart, mEnd,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                mStart = -1;
                mEnd = -1;
            }

            Selection.removeSelection(buffer);
            return false;
        }
        // }

        // return false;
    }

    @Override
    public boolean hasFocusable() {
        return false;
    }

    public void setHtmlText(String str, int start, int end, int color) {
        Spannable span = setClickableSpan(str, start, end, color);
        if (null != span) {
            setMovementMethod(LinkMovementMethod.getInstance());
            setText(span);
        } else {
            setText(str);
        }
    }

    /**
     * 默认是 BLUE
     * @param str
     * @param start
     * @param end
     */
    public void setHtmlText(String str, int start, int end) {
        setHtmlText(str,start,end,Color.BLUE);
    }

    private Spannable setClickableSpan(String text, int start, int end, int color) {
        if (TextUtils.isEmpty(text)) {
            return null;
        }
        SpannableString span = new SpannableString(text);
        MyClickSpan myClickSpan = new MyClickSpan(color);
        span.setSpan(myClickSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return span;
    }

    public class MyClickSpan extends ClickableSpan {

        private int color;

        public MyClickSpan(int color) {
            this.color = color;
        }

        @Override
        public void onClick(View arg0) {
            Spannable spannable = (Spannable) ((TextView) arg0).getText();
            Selection.removeSelection(spannable);
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setUnderlineText(false);
            ds.setColor(color);
        }
    }
}
