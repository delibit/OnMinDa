package com.newthread.android.dialog;



import android.app.Activity;
import android.content.Context;
import android.content.Intent;


public class DiaLogManager {
	

	public static void showDiaLog(Activity activity,OnDiaLogListener diaLogListener, DiaLogConfig diaLogConfig) {
		Class<?> dialogType = diaLogConfig.getDialogClassType();
		DiaLogListenerManager.getInstance().addDiaLogListener(diaLogListener, diaLogConfig);
		final Intent intent = new Intent(activity, dialogType);
		intent.putExtra(DiaLogConfig.INTENT_EXTRA_CONFIG_NAME, diaLogConfig);
		activity.startActivity(intent);
	}

	public static void showDiaLog(Context context,OnDiaLogListener diaLogListener,DiaLogConfig diaLogConfig) {
		Class<?> dialogType = diaLogConfig.getDialogClassType();
		DiaLogListenerManager.getInstance().addDiaLogListener(diaLogListener, diaLogConfig);
		Intent aintent= new Intent(context,dialogType );
    	aintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    	aintent.putExtra(DiaLogConfig.INTENT_EXTRA_CONFIG_NAME, diaLogConfig);
    	context.startActivity(aintent);
	}

}
