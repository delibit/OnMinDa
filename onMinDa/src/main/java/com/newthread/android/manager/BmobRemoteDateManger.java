package com.newthread.android.manager;

import android.content.Context;
import cn.bmob.v3.listener.DeleteListener;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;
import com.newthread.android.bean.remoteBean.RemoteObject;
import com.newthread.android.manager.iMangerService.IRemoteService;

/**
 * Created by jindongping on 15/5/27.
 */
public class BmobRemoteDateManger<T extends RemoteObject> implements IRemoteService<T> {
    private Context context;
    private IRemoteService iRemoteService;

    @Override
    public void add(T t, SaveListener saveListener) {
        iRemoteService = t.getImpl(context);
        iRemoteService.add(t, saveListener);
    }

    @Override
    public void update(T t, UpdateListener updateListener) {
        iRemoteService = t.getImpl(context);
        iRemoteService.update(t, updateListener);
    }


    @Override
    public void querry(T t, FindListener<T> findListener) {
        iRemoteService = t.getImpl(context);
        iRemoteService.querry(t, findListener);
    }

    @Override
    public void delete(T t, DeleteListener deleteListener) {
        iRemoteService = t.getImpl(context);
        iRemoteService.delete(t, deleteListener);
    }


    private static class BombRemoteDateMangerHodler {

        private static BmobRemoteDateManger getInstance(Context context) {
            return new BmobRemoteDateManger(context);
        }
    }

    public static BmobRemoteDateManger getInstance(Context context) {
        return BombRemoteDateMangerHodler.getInstance(context);
    }

    private BmobRemoteDateManger(Context context) {
        this.context = context;

    }

}
