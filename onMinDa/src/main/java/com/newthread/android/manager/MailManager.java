package com.newthread.android.manager;


import com.newthread.android.mail.*;

/**
 * Created by jindongping on 14-9-17.
 */
public class MailManager {
    private static MailManager instance;
    public static MailManager getInstance() {
        synchronized (MailManager.class) {
            if (instance == null) {
                instance = new MailManager();
            }
        }
        return instance;
    }
    public void sendEmail(String form,String formPassWd,String to,String subject, String content, String attachmentPath,MailSender.CallBack callBack) {
        MailAuthor mailAuthor = new MailAuthor(form,formPassWd);
        MailContent mailContent = new MailContent(subject, content, attachmentPath);
        MailMs mailMs = new MailMs(mailAuthor, mailContent, to);
        MailSender mailSender = new MailSender(new MySetting().getMailSetting(mailAuthor.getAddress()));
        mailSender.sendMail(mailMs,callBack);
    }
}
