package com.newthread.android.bean;

import net.tsz.afinal.annotation.sqlite.Id;
import net.tsz.afinal.annotation.sqlite.Table;

/**
 * Created by jindongping on 15/6/4.
 */
@Table(name = "studentUser")
public class StudentUserLocal {

    @Id
    private String id;
    private String studentId;//学号
    private String name;//姓名
    private String sex;//性别
    private Integer age;


    private String qq;
    private String weiXin;
    private String mobPhone;

    private String brithDay;//生日
    private String province;//省份
    private String politicsStatus;//政治面貌
    private String nation;//民族
    private String departMent;//院系
    private String marjar;//专业
    private String nianji;//年级
    private String classNumber;//班级号
    private String whereForm;//来自哪里
    private String liveAddress;//宿舍

    private String headPhotoUrl;


    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getWeiXin() {
        return weiXin;
    }

    public void setWeiXin(String weiXin) {
        this.weiXin = weiXin;
    }

    public String getMobPhone() {
        return mobPhone;
    }

    public void setMobPhone(String mobPhone) {
        this.mobPhone = mobPhone;
    }

    public String getBrithDay() {
        return brithDay;
    }

    public void setBrithDay(String brithDay) {
        this.brithDay = brithDay;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPoliticsStatus() {
        return politicsStatus;
    }

    public void setPoliticsStatus(String politicsStatus) {
        this.politicsStatus = politicsStatus;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getDepartMent() {
        return departMent;
    }

    public void setDepartMent(String departMent) {
        this.departMent = departMent;
    }

    public String getMarjar() {
        return marjar;
    }

    public void setMarjar(String marjar) {
        this.marjar = marjar;
    }

    public String getNianji() {
        return nianji;
    }

    public void setNianji(String nianji) {
        this.nianji = nianji;
    }

    public String getClassNumber() {
        return classNumber;
    }

    public void setClassNumber(String classNumber) {
        this.classNumber = classNumber;
    }

    public String getWhereForm() {
        return whereForm;
    }

    public void setWhereForm(String whereForm) {
        this.whereForm = whereForm;
    }

    public String getLiveAddress() {
        return liveAddress;
    }

    public void setLiveAddress(String liveAddress) {
        this.liveAddress = liveAddress;
    }

    public String getHeadPhotoUrl() {
        return headPhotoUrl;
    }

    public void setHeadPhotoUrl(String headPhotoUrl) {
        this.headPhotoUrl = headPhotoUrl;
    }

    @Override
    public String toString() {
        return "StudentUserLocal{" +
                "id='" + id + '\'' +
                "studentId=" + studentId + '\'' +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", age=" + age +
                ", qq='" + qq + '\'' +
                ", weiXin='" + weiXin + '\'' +
                ", mobPhoto='" + mobPhone + '\'' +
                ", brithDay='" + brithDay + '\'' +
                ", province='" + province + '\'' +
                ", politicsStatus='" + politicsStatus + '\'' +
                ", nation='" + nation + '\'' +
                ", departMent='" + departMent + '\'' +
                ", marjar='" + marjar + '\'' +
                ", nianji='" + nianji + '\'' +
                ", classNumber='" + classNumber + '\'' +
                ", whereForm='" + whereForm + '\'' +
                ", liveAddress='" + liveAddress + '\'' +
                ", headPhotoUrl='" + headPhotoUrl + '\'' +
                '}';
    }

    public StudentUserLocal(String id, String studentId, String name, String sex, Integer age, String qq, String weiXin, String mobPhone, String brithDay, String province, String politicsStatus, String nation, String departMent, String marjar, String nianji, String classNumber, String whereForm, String liveAddress, String headPhotoUrl) {
        this.id = id;
        this.studentId = studentId;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.qq = qq;
        this.weiXin = weiXin;
        this.mobPhone = mobPhone;
        this.brithDay = brithDay;
        this.province = province;
        this.politicsStatus = politicsStatus;
        this.nation = nation;
        this.departMent = departMent;
        this.marjar = marjar;
        this.nianji = nianji;
        this.classNumber = classNumber;
        this.whereForm = whereForm;
        this.liveAddress = liveAddress;
        this.headPhotoUrl = headPhotoUrl;
    }

    public StudentUserLocal() {
    }
}
